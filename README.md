## Setup Localhost
Clone the repo and install the dependencies.

```bash
git clone https://gitlab.com/Nugroh03/example-backend-fazztrack.git
```
```bash
npm install
```

To start the express server, run the following

```bash
  "scripts": {
    "dev": "nodemon ./index.js",
    "test": "mocha tests/*.js --exit"
  },
```

running with nodemon

```bash
node index.js
npm run dev
```

running testing with mocha

```bash
npm run test
```

## Containerization
```
docker build -t faztrack-nugroho:1.0 .
docker tag faztrack-nugroho:1.0 nugroh03/faztrack-nugroho:latest
docker push nugroh03/faztrack-nugroho:latest
docker run -p 3000:3000 -d nugroh03/faztrack-nugroho:latest
```


## KUBERNETES (MINIKUBE)
```
minikube status
minikube start

kubectl apply -f kubernetes/deployment.yaml
kubectl apply -f kubernetes/service.yaml


kubectl get po

NAME                          READY   STATUS    RESTARTS   AGE
nugroho-app-7d57bd494-mmc5k   1/1     Running   0          5s


kubectl get svc

kubernetes    ClusterIP   12.678.3.3     <none>        443/TCP          172m
nugroho-app   NodePort    10.96.185.81   <none>        3000:31500/TCP   15m


minikube service nugroho-app

|-----------|-------------|-------------|---------------------------|
| NAMESPACE |    NAME     | TARGET PORT |            URL            |
|-----------|-------------|-------------|---------------------------|
| default   | nugroho-app |        3000 | http://192.168.49.2:31500 |
|-----------|-------------|-------------|---------------------------|
🏃  Starting tunnel for service nugroho-app.
|-----------|-------------|-------------|------------------------|
| NAMESPACE |    NAME     | TARGET PORT |          URL           |
|-----------|-------------|-------------|------------------------|
| default   | nugroho-app |             | http://127.0.0.1:49752 |

``` 

# RESULT

![Postman Nugroho 1](https://gitlab.com/Nugroh03/example-backend-fazztrack/-/raw/master/screen/nugroho1.png?raw=true)

![Postman Nugroho 2](https://gitlab.com/Nugroh03/example-backend-fazztrack/-/raw/master/screen/nugroho2.png?raw=true)

![Minikube](https://gitlab.com/Nugroh03/example-backend-fazztrack/-/raw/master/screen/nugroho3.png?raw=true)